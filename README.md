# Software Engineering - Crypto Exchange 

## Submission Guide

**You are not required to prepare a working solution** ahead of the call. 
This guide is only to help you prepare for the conversation, and we are expecting to collaborate with you during the call. 
Whiteboarding allows us to exchange ideas and understand your thought process. Feel free to ask many questions as you need for you to highlight your skills.

**Candidates do not need to have a working knowledge of cryptocurrencies, blockchain technology or web3 tooling** for this challenge. This is a whiteboarding challenge on designing a data aggregation system from multiple 3rd party sources, similar to flight and hotel price aggregators.  


## Challenge

You are given a task to design an application to aggregate market data for various **Coins** (assets, cryptocurrencies) from multiple **Exchanges** (data price providers). 

There are many **Exchanges**, and each **Exchange** provides their own price feed to retrieve the current price of a **Coin**. The price for a coin is the average price of the Coin across all exchanges. For example:

- Binance shows a price of $25000 for BTC/USD
- Coinbase shows a price of $26000 for BTC/USD 
- Kraken shows a price of $25500 for BTC/USD 

You want to display the price of BTC as $25500. To simplify this exercise, you can assume all cryptocurrencies are only trading against USD base. 

Your **Users** will use your app to:

1. Find which Exchange provides the lowest price for a given **Coin**
2. Find the _best_ price across all Exchange for a given **Coin** 
3. View the daily min/max prices over the last 7 days.

- Each **Exchange** provides the latest price data for a given **Coin** with basic attributes - last traded price, last 24 hour volume, high, low, etc.
- Each **Exchange** API returns response with a different JSON payload structure e.g. key names, value data type e.g. [Binance](https://binance-docs.github.io/apidocs/spot/en/#test-new-order-trade) and [Coinbase](https://docs.cloud.coinbase.com/exchange/reference/exchangerestapi_getproductstats)

You should also consider that each **Exchange** API may require different authetication methods and fail differently.


As the lead developer for this project, you are tasked to conduct a planning session with the team. You are anticipating these questions:

### Code Architecture

- How would you design the data pipeline to ingest and process different **Exchange** data sources?
- How would you model your data to allow for historical retrieval and computation of **Coins**?
- How would you present your data to users as an API?


### System Design

- How does your design tolerate external **Exchange** API failures?
- How does your design handle changes to data models such as **Coins** attributes? 
- How will you deploy updates to the application on production? What tools will you be using?


## Preparation Guide

If you are preparing for a remote whiteboarding session, here are a few tips to help you have an effective interview:

1. Prepare tools such as note-taking, online REPL, online whiteboards that will help you illustrate your work. We recommend [hackmd](https://hackmd.io/), Google Docs, [diagrams.net](https://app.diagrams.net/), [dbdiagram](https://dbdiagram.io/home), [repl.it](https://replit.com/) if they would be useful. 

2. Reduce external distractions and other factors that may disrupt your interview experience. Feel free to reach out to the panel to reschedule your interview if you need to. Communication is key for us as a team.

3. Warm yourself up and check out our [stackshare](https://stackshare.io/coingecko) and [blog](https://blog.coingecko.com/category/engineering/) to see what we have been up to.


## See you in the interview!

We are looking forward to the call. Do keep your recruiters updated for any unforeseen changes leading up to the call.

